package org.tio.showcase.http.model;

import lombok.Data;

import java.util.Objects;

/**
 * 群组信息
 */
@Data
public class Group {

    private String username;        // 群主
    private String groupName;       // 群名
    private String groupAvatarUrl;  // 群头像
    private String groupNick;       // 群描述
    private String groupAddress;    // 群地址
    private String creationTime;    // 添加时间
    private String id;              // mongodb 自动生成的ID
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(username, group.username) &&
                Objects.equals(groupName, group.groupName) &&
                Objects.equals(groupAvatarUrl, group.groupAvatarUrl) &&
                Objects.equals(groupNick, group.groupNick) &&
                Objects.equals(groupAddress, group.groupAddress) &&
                Objects.equals(creationTime, group.creationTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, groupName, groupAvatarUrl, groupNick, groupAddress, creationTime);
    }
}
