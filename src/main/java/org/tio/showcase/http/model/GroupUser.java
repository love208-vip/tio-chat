package org.tio.showcase.http.model;

import lombok.Data;

/**
 * 群用户关系
 */
@Data
public class GroupUser {
    private String groupId;     // 群ID
    private String userId;      // 用户ID
    private String creationTime;// 加群时间
    private String type;        // 是否退群

}
