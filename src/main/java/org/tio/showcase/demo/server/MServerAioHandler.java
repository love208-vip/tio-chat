package org.tio.showcase.demo.server;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.ChannelContext;
import org.tio.core.Node;
import org.tio.core.Tio;
import org.tio.core.TioConfig;
import org.tio.core.exception.AioDecodeException;
import org.tio.core.intf.Packet;
import org.tio.server.intf.ServerAioHandler;

import java.nio.ByteBuffer;


public class MServerAioHandler implements ServerAioHandler {


    private static final Logger logger = LoggerFactory.getLogger(MServerAioHandler.class);

    /**
     * 消息解码
     * @param byteBuffer
     * @param limit
     * @param position
     * @param readableLength
     * @param channelContext
     * @return
     * @throws AioDecodeException
     */
    public Packet decode(ByteBuffer byteBuffer, int limit, int position, int readableLength, ChannelContext channelContext) throws AioDecodeException {

        logger.debug("inside decode...");

        if (readableLength < ServerPacket.PACKET_HEADER_LENGTH) {
            return null;
        }

        int bodyLength = byteBuffer.getInt();

        if (bodyLength < 0) {
            throw new AioDecodeException("body length[" + bodyLength + "] is invalid. romote: " + channelContext.getServerNode());
        }

        int len = bodyLength + ServerPacket.PACKET_HEADER_LENGTH;
        if (len > readableLength) {
            return null;
        } else {
            byte[] bytes = new byte[len];

            int i = 0;

            while(true){

                if(byteBuffer.remaining() == 0){
                    break;
                }
                byte b = byteBuffer.get();
                bytes[i++] = b;
            }

            ServerPacket serverPacket = new ServerPacket();
            serverPacket.setBody(bytes);
            return serverPacket;
        }
    }

    public ByteBuffer encode(Packet packet, TioConfig groupContext, ChannelContext channelContext) {
        logger.info("inside encode...");

        ServerPacket serverPacket = (ServerPacket) packet;

        byte[] body = serverPacket.getBody();

        int bodyLength = 0;
        if(body != null){
            bodyLength = body.length;
        }

        ByteBuffer byteBuffer = ByteBuffer.allocate(bodyLength + ServerPacket.PACKET_HEADER_LENGTH);
        byteBuffer.order(groupContext.getByteOrder());
        byteBuffer.putInt(bodyLength);

        if (body != null) {
            byteBuffer.put(body);
        }

        return byteBuffer;
    }

    public void handler(Packet packet, ChannelContext channelContext) throws Exception {
        logger.debug("inside handler...");

//        channelContext.setServerNode(new Node("127.0.0.1", ServerPacket.PORT));

        ServerPacket serverPacket = (ServerPacket) packet;

        byte[] body = serverPacket.getBody();
        if(body != null){
            String bodyStr = new String(body, serverPacket.CHARSET);
            System.out.println("收到消息：" + bodyStr);
            //ServerPacket serverPacket1 = new ServerPacket();
            //serverPacket.setBody(("receive from ["+ channelContext.getClientNode() + "]: " + bodyStr).getBytes("utf-8"));
            serverPacket.setBody(("收到了你的消息，你的消息是:" + bodyStr).getBytes(serverPacket.CHARSET));

            Tio.send(channelContext, serverPacket);
        }

    }
}