package org.tio.showcase.demo.server;

import org.tio.core.intf.Packet;

/**
 * @author tsinghua
 * @date 2018/6/20
 */
public class ServerPacket extends Packet {
    public static final int HEADER_LENGTH = 4;//消息头的长度
    public static final String CHARSET = "utf-8";
    public static final Integer PACKET_HEADER_LENGTH = 4;
    public static final Integer PORT = 8999;

    byte[] body;//数据

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }
}
