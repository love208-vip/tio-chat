package org.tio.showcase.demo;

import org.tio.client.ClientChannelContext;
import org.tio.client.ClientTioConfig;
import org.tio.client.TioClient;
import org.tio.core.Node;
import org.tio.core.Tio;
import org.tio.server.ServerTioConfig;
import org.tio.server.TioServer;
import org.tio.showcase.demo.client.ClientPacket;
import org.tio.showcase.demo.client.MClientAioHander;
import org.tio.showcase.demo.client.MClientAioListener;
import org.tio.showcase.demo.server.MServerAioHandler;
import org.tio.showcase.demo.server.MServerAioListener;
import org.tio.utils.Threads;

/**
 * Hello world!
 */
public class App {

    public static ServerTioConfig serverTioConfig    = null;
    public static void main(String[] args) throws Exception {


        serverTioConfig = new ServerTioConfig("tio-server", new MServerAioHandler(), new MServerAioListener(), Threads.getTioExecutor(),
                Threads.getGroupExecutor());

        TioServer server = new TioServer(serverTioConfig);

        TioServer tioServer = new TioServer(serverTioConfig);

        server.start("127.0.0.1", 8999);

        //////////// 上面是服务端，下面的客户端启动 /////////////

        ClientTioConfig clientGroupContext = new ClientTioConfig(new MClientAioHander(), new MClientAioListener());

        TioClient tioClient = new TioClient(clientGroupContext);

        ClientChannelContext clientChannelContext = tioClient.connect(new Node("127.0.0.1", 8999));

        ClientPacket clientPacket = new ClientPacket();
        clientPacket.setBody("hello,t-tio".getBytes("utf-8"));

        Tio.send(clientChannelContext, clientPacket);
    }
}