package org.tio.showcase.ai;

import cn.xsshome.taip.nlp.TAipNlp;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Date;
import java.util.Random;

public class RobotChat {


    /**
     * 闲聊，基于腾讯AI
     * 开源项目：https://gitee.com/xshuai/taip.git
     *
     * @param msg
     * @return
     * @throws Exception
     */
    public static String getRobotMsg(String msg) throws Exception {

        TAipNlp aipNlp = new TAipNlp("1106980094", "BVgJWBjVtagLSBhf");
        String session = new Date().getTime() / 1000 + "";
        String result = aipNlp.nlpTextchat(session, msg);//基础闲聊
        if (JSON.parseObject(result).getString("msg").equals("ok")) {
            String json = JSON.parseObject(result).getString("data");
            return JSON.parseObject(json).getString("answer");
        } else {
            return "额，您这就难到我了。咚，已挂机！";
        }
    }

    private static long getNum(int digit) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < digit; i++) {
            if (i == 0 && digit > 1)
                str.append(new Random().nextInt(9) + 1);
            else
                str.append(new Random().nextInt(10));
        }
        return Long.valueOf(str.toString());
    }

    public static String getPicUrl() {
        int digit = new Random().nextInt(10);
        String url = "https://api.gxusb.com/qq/?s=640&qq=" + getNum(digit);
        return url;
    }
}
